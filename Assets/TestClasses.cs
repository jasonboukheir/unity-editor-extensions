﻿using System;
using UnityEditorExtensions;

public interface IRef { }

[Serializable]
[ProvideSourceInfo]
public class ProvidesSource : IRef
{
    public bool toggleMe;
}

[Serializable]
public class DoesNotProvideSource : IRef
{
    public string writeMe;
}
