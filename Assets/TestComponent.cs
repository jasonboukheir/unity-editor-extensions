﻿using UnityEngine;
using UnityEditorExtensions;

public class TestComponent : MonoBehaviour
{
    [SerializeReference]
    public IRef ref1;

    [SerializeReference]
    [ShowValueType]
    public IRef ref2;
}
