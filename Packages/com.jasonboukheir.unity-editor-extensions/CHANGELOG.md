# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.5-preview] - 2020-04-11
### Fixed
- Case in SerializedProperty.GetValue where a private field could not be returned.

## [0.0.4-preview] - 2020-03-15
### Added
- ShowValueTypeAttribute that shows a type drawer for the serialize reference field. This can be used to instantiate a derived serialize reference in editor.

### Fixed
- Type field now properly double clicks.
- Type field style is now exactly same as object field.

## [0.0.3-preview] - 2020-03-14
### Added
- SelectTypeWindow for searching and selecting a type out of a given set of types.
- TypeField util to select and display types in inspector.
- ProvideSourceInfoAttribute to allow the editor to select or open types if given a ProvideSourceInfoAttribute.

## [0.0.2-preview] - 2020-03-06
### Fixed
- Project structure for publishing and validation

## [0.0.1] - 2020-03-04
### Added
- EnumerableExtension methods ElementAt and SkipLast
- SerializedPropertyExtension methods GetValue