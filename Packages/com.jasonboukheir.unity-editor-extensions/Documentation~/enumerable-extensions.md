# EnumerableExtensions

## `ElementAt(int index)`
Provides the element in an `IEnumerable` at the given index.

## `SkipLast(int count)`
Returns an `IEnumerable<TResult>` that has `count` elements removed from the end.