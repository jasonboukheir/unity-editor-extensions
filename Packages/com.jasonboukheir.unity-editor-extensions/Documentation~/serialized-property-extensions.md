# SerializedPropertyExtensions

## `GetValue(Func<IEnumerable<string>, IEnumerable<string>> pathModifier = null)`
Returns the object value of the `SerializedProperty` at its `propertyPath`. A `pathModifier` can be given that can act on the split `propertyPath`. 