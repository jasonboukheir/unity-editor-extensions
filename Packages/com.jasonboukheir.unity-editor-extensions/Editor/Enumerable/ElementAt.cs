﻿using System;
using System.Collections;

namespace UnityEditorExtensions
{
    public static partial class EnumerableExtensions
    {
        public static object ElementAtOrDefault(this IEnumerable sequence, int index)
        {
            if (sequence == null) throw new ArgumentNullException(nameof(sequence));
            foreach (var element in sequence)
            {
                if (index == 0) return element;
                index--;
            }
            return null;
        }
    }
}
