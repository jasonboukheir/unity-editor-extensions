﻿using System;
using UnityEditor;
using static UnityEditorExtensions.TypeExtensions;

namespace UnityEditorExtensions
{
    public static partial class SerializedPropertyExtensions
    {
        public static Type GetManagedReferenceFieldType(this SerializedProperty property)
        {
            return GetTypeFromManagedReferenceFullTypename(property.managedReferenceFieldTypename, out var type)
                ? type
                : null;
        }
    }
}
