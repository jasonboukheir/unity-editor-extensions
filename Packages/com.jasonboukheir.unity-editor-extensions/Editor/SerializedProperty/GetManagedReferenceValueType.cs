﻿using System;
using UnityEditor;
using static UnityEditorExtensions.TypeExtensions;

namespace UnityEditorExtensions
{
    public static partial class SerializedPropertyExtensions
    {
        public static Type GetManagedReferenceValueType(this SerializedProperty property)
        {
            return GetTypeFromManagedReferenceFullTypename(property.managedReferenceFullTypename, out var type)
                ? type
                : null;
        }
    }
}
