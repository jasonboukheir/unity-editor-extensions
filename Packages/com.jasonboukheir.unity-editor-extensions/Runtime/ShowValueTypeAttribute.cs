﻿using System.Diagnostics;
using UnityEngine;

namespace UnityEditorExtensions
{
    [Conditional("UNITY_EDITOR")]
    public class ShowValueTypeAttribute : PropertyAttribute { }
}
