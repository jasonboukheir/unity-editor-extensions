﻿using System;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace UnityEditorExtensions.Editor.Tests
{
    public class SerializedPropertyExtensionsTest
    {
        [Serializable]
        public class SerializableObject
        {
            public bool NestedField = true;
            public bool[] NestedArrayField = new[] { true, false };
        }

        public class MockAsset : ScriptableObject
        {
            public int IntField = 1;

            public string StringField = "Hello World!";

            public bool[] ArrayField = new[] { false, true };

            public SerializableObject ObjectField = new SerializableObject();

#pragma warning disable 4014
            [SerializeField]
            private bool privateField = true;
#pragma warning restore 4014
        }

        public class SerializedPropertyTestCase
        {
            public string PropertyPath { get; }

            public object Expected { get; }

            public SerializedProperty Property =>
                new SerializedObject(ScriptableObject.CreateInstance<MockAsset>())
                    .FindProperty(PropertyPath);

            public SerializedPropertyTestCase(string propertyPath, object expected)
            {
                PropertyPath = propertyPath;
                Expected = expected;
            }

            public override string ToString() => $"{PropertyPath} = {Expected}";
        }

        public static object[] GetValueTestCases = new object[]
        {
            new SerializedPropertyTestCase(
                "IntField",
                1
                ),
            new SerializedPropertyTestCase(
                "StringField",
                "Hello World!"
                ),
            new SerializedPropertyTestCase(
                "ArrayField.Array.data[1]",
                true
                ),
            new SerializedPropertyTestCase(
                "ObjectField.NestedField",
                true
                ),
            new SerializedPropertyTestCase(
                "ObjectField.NestedArrayField.Array.data[0]",
                true
                ),
            new SerializedPropertyTestCase(
                "privateField",
                true
                ),
        };

        [Test]
        [TestCaseSource(nameof(GetValueTestCases))]
        public void GetValueShouldEqualSetValue(SerializedPropertyTestCase propertyCase)
        {
            Assert.AreEqual(propertyCase.Expected, propertyCase.Property.GetValue());
        }
    }
}
