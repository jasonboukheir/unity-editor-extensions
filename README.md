# To Install

## Via scoped registries
Follow https://docs.upm-packages.dev/en/usage to set up your scoped registry,
then add `com.jasonboukheir.unity-editor-extensions` to your dependencies:

```
"dependencies": {
    "com.jasonboukheir.unity-editor-extensions": "latest"
    ...
}
```

## Via github url
Add the following to your dependencies:
```
"dependencies": {
    "com.jasonboukheir.unity-editor-extensions": "https://gitlab.com/jasonboukheir/unity-editor-extensions.git?path=/Packages/com.jasonboukheir.unity-editor-extensions",
    ...
}
```

# Using

## SerializeReference Value Type Drawer
1. Add the `[ShowValueType]` attribute on your property marked with `[SerializeReference]`
2. A type field will appear in the inspector next to your property.
3. You can select a type to instantiate a new instance of that type, or select "None" to set the value to null.
